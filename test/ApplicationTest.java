import controllers.Application;
import controllers.Examples;
import models.exceptions.InadequateParameterException;
import org.junit.Test;
import play.twirl.api.Content;

import static org.fest.assertions.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import static play.test.Helpers.contentAsString;
import static play.test.Helpers.contentType;


/**
*
* Simple (JUnit) tests that can call all parts of a play app.
* If you are interested in mocking a whole application, see the wiki for more details.
*
*/
public class ApplicationTest {

    /**
     * Story: Application should have a default List created when server start.
     *
     * As a Controller
     * I want that when I start system, a default List is created and set do display
     * So then I can make tests with these list at display.
     *
     * Given that server were started
     * When requisitions of List are made
     * Then this requisitions comprehend a default List in System.
     */
    @Test
    public void shouldApplicationDoASearch() {

        Application.getResultFromSearch("sdasdasdasmdlkanslkdansklasnldkjansl", "keyword", 0);
    }

    /**
     * Story: Application should have a default List created when server start.
     *
     * As a Controller
     * I want that when I start system, a default List is created and set do display
     * So then I can make tests with these list at display.
     *
     * Given that server were started
     * When requisitions of List are made
     * Then this requisitions comprehend a default List in System.
     */
    @Test
    public void shouldApplicationHaveADefaultList() {
        Application.updateAds();
        Content html = views.html.index.render();
        try {
            assertEquals("Violeira pronta pra tocar", Application.getAd(0).getTitle());
            assertEquals("Cascavel", Application.getAd(1).getCity());
        } catch (InadequateParameterException e) {
            fail();
        }
    }
}

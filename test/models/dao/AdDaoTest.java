package models.dao;

import controllers.Examples;
import junit.framework.Assert;
import models.items.Ad;
import org.junit.Test;

import static junit.framework.Assert.assertEquals;

/**
 * Created by WIN7 on 25/10/2015.
 */
public class AdDaoTest {

    /**
     * Story: Addition and Delection of Ads in database must be possible
     *
     * As a System using database with Ads
     * I want that I'm adding or deleting an Ad on my database, no error occur
     * So then I can assure that I've have added or delected correctly.
     *
     * Given that an Ad exist
     * When I put or delect and Ad on database
     * Then I should be able to confirm the operation looking on database.
     */
    @Test
    public void shouldBeEqualViaID(){
        Ad ad01 = Examples.getAdAmandaFromGuarulhos();
        AdDao dao = new AdDao();
        dao.add(ad01);

        Ad ad02 = dao.getAdWithID(ad01.getID());
        assertEquals(ad02.getID(), ad01.getID());
        assertEquals(ad02.getCode(), ad01.getCode());

        dao.remove(ad01);
    }

    public void addDefaultAds(){
        AdDao dao = new AdDao();
        dao.add(Examples.getAdAmandaFromGuarulhos());
        dao.add(Examples.getAdBrunoFromCascavel());
        dao.add(Examples.getAdDaniloFromCarapicuiba());
        dao.add(Examples.getAdEmillyFromRioDeJaneiro());
        dao.add(Examples.getAdGabrielaFromItauna());
        dao.add(Examples.getAdJoaoFromTucurui());
        dao.add(Examples.getAdLauraFromBauru());
        dao.add(Examples.getAdManuelaFromAnapolis());
        dao.add(Examples.getAdSophiaFromSalvador());
        dao.add(Examples.getAdGuilhermeFromJuizDeFora());
    }
}

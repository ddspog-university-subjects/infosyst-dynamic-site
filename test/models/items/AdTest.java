package models.items;

import controllers.Examples;
import models.exceptions.InadequateParameterException;
import models.items.Ad;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;


/**
*
* Story: [Model]
* As a [Role]
* I want [Feature]
* So that I receive [Value]
*
* Given [Context]
* When [Event Occurs]
* Then  [Outcome]
*/
public class AdTest {

    private String[] instrSet01 = new String[]{ "Violão", "Guitarra", "Violino", "Triângulo" };
    private String[] niceSty01 = new String[]{"Trash Metal", "Hard Rock", "Metal"};
    private String[] badSty01 = new String[]{"Forró", "Lambada"};
    private String[] contactPair01 = new String[]{"", "ddspag@gmail.com"};
    private String[] contactPair02 = new String[]{"dnnis.d.dantas", ""};
    private String[] contactPair03 = new String[]{"jacob.f.neto", "jacobneto@yahoo.com.br"};

    /**
     * Story: Ad should have equals compare via ID
     *
     * As a System using Ads
     * I want that when verify that an Ad is equal to another, the primal parameter comparing is ID
     * So then I can assure difference between Ads created in different times.
     *
     * Given that two Ads exist
     * When a verification that the two are equal is called
     * Then the result is true only if they share same ID.
     */
    @Test
    public void shouldBeEqualViaID(){
        Ad ad01 = Examples.getSecureAd("a", "a", "a", "a", true, contactPair01, Arrays.asList(instrSet01),
            Arrays.asList(niceSty01), Arrays.asList(badSty01));
        Ad ad02 = Examples.getSecureAd("a", "a", "a", "a", true, contactPair01, Arrays.asList(instrSet01),
            Arrays.asList(niceSty01), Arrays.asList(badSty01));
        Ad ad03 = ad01;
        assertEquals(ad01, ad03);
        assertNotEquals(ad01, ad02);
        assertNotEquals(ad02, ad03);
    }

    /**
     * Story: Ad have a Q&A section
     *
     * As a System using Ads
     * I want that when I create Ads, a Q&A section should be created as a list of questions
     * with space to answers.
     * So then I can maintain a system to create question and leave option to answers.
     *
     * Given that an Ad is being created
     * When a Ad was correctly created
     * Then the ad should have a non-null List of Questions with Answers.
     */
    @Test
    public void shouldHaveQASection() {
        String qStr01 = "Are you interested in work on which period?";
        String qStr02 = "Are you Woman or Man?";
        String aStr01 = "Don't know";
        String aStr02 = "Man";
        try {
            Ad a = new Ad();
            a.setTitle("a");
            a.setDescription("a");
            a.setCity("a");
            a.setNeighborhood("a");
            a.setInterestOnBand(true);
            a.setContact(contactPair01);
            a.setInstruments(Arrays.asList(instrSet01));
            a.setNiceStyles(Arrays.asList(niceSty01));
            a.setBadStyles(Arrays.asList(badSty01));

            assertEquals(a.getNumberOfQuestions(), 0);
            a.addQuestion(qStr01);
            assertEquals(a.getNumberOfQuestions(), 1);
            assertEquals(a.getQuestion(0), qStr01);
            assertFalse(a.wasAnswered(0));
            assertEquals(a.getAnswer(0), "");
            a.answer(0, aStr01);
            assertTrue(a.wasAnswered(0));
            assertEquals(a.getAnswer(0), aStr01);
            assertFalse(a.isHidden(0));

            a.addQuestion(qStr02);
            assertEquals(a.getNumberOfQuestions(), 2);
            assertEquals(a.getQuestion(1), qStr02);
            assertFalse(a.wasAnswered(1));
            a.answer(1, aStr02);
            assertTrue(a.wasAnswered(1));
            assertEquals(a.getAnswer(1), aStr02);
            assertFalse(a.isHidden(1));

            a.hide(0);
            assertTrue(a.isHidden(0));
            assertEquals(a.getNumberOfQuestions(), 2);
            assertEquals(a.getQuestion(0), qStr01);
            assertTrue(a.wasAnswered(0));
            assertEquals(a.getAnswer(0), aStr01);

        } catch (Exception e) {
            fail();
        }
        try{
            Ad a = new Ad();
            a.setTitle("a");
            a.setDescription("a");
            a.setCity("a");
            a.setNeighborhood("a");
            a.setInterestOnBand(true);
            a.setContact(contactPair01);
            a.setInstruments(Arrays.asList(instrSet01));
            a.setNiceStyles(Arrays.asList(niceSty01));
            a.setBadStyles(Arrays.asList(badSty01));
            a.getQuestion(0);
            fail();
        } catch (InadequateParameterException e) {
            assertEquals(e.getMessage(), "Question 0 don't exist");
        } catch (Exception e){
            fail();
        }
        try{
            Ad a = new Ad();
            a.setTitle("a");
            a.setDescription("a");
            a.setCity("a");
            a.setNeighborhood("a");
            a.setInterestOnBand(true);
            a.setContact(contactPair01);
            a.setInstruments(Arrays.asList(instrSet01));
            a.setNiceStyles(Arrays.asList(niceSty01));
            a.setBadStyles(Arrays.asList(badSty01));
            a.addQuestion(qStr01);
            a.getQuestion(1);
            fail();
        } catch (InadequateParameterException e) {
            assertEquals(e.getMessage(), "Question 1 don't exist");
        } catch (Exception e){
            fail();
        }
        try{
            Ad a = new Ad();
            a.setTitle("a");
            a.setDescription("a");
            a.setCity("a");
            a.setNeighborhood("a");
            a.setInterestOnBand(true);
            a.setContact(contactPair01);
            a.setInstruments(Arrays.asList(instrSet01));
            a.setNiceStyles(Arrays.asList(niceSty01));
            a.setBadStyles(Arrays.asList(badSty01));
            a.getAnswer(0);
            fail();
        } catch (InadequateParameterException e) {
            assertEquals(e.getMessage(), "Question 0 don't exist");
        } catch (Exception e){
            fail();
        }
        try{
            Ad a = new Ad();
            a.setTitle("a");
            a.setDescription("a");
            a.setCity("a");
            a.setNeighborhood("a");
            a.setInterestOnBand(true);
            a.setContact(contactPair01);
            a.setInstruments(Arrays.asList(instrSet01));
            a.setNiceStyles(Arrays.asList(niceSty01));
            a.setBadStyles(Arrays.asList(badSty01));
            a.addQuestion(qStr01);
            a.getAnswer(1);
            fail();
        } catch (InadequateParameterException e) {
            assertEquals(e.getMessage(), "Question 1 don't exist");
        } catch (Exception e){
            fail();
        }
        try{
            Ad a = new Ad();
            a.setTitle("a");
            a.setDescription("a");
            a.setCity("a");
            a.setNeighborhood("a");
            a.setInterestOnBand(true);
            a.setContact(contactPair01);
            a.setInstruments(Arrays.asList(instrSet01));
            a.setNiceStyles(Arrays.asList(niceSty01));
            a.setBadStyles(Arrays.asList(badSty01));
            a.isHidden(0);
            fail();
        } catch (InadequateParameterException e) {
            assertEquals(e.getMessage(), "Question 0 don't exist");
        } catch (Exception e){
            fail();
        }
        try{
            Ad a = new Ad();
            a.setTitle("a");
            a.setDescription("a");
            a.setCity("a");
            a.setNeighborhood("a");
            a.setInterestOnBand(true);
            a.setContact(contactPair01);
            a.setInstruments(Arrays.asList(instrSet01));
            a.setNiceStyles(Arrays.asList(niceSty01));
            a.setBadStyles(Arrays.asList(badSty01));
            a.addQuestion(qStr01);
            a.isHidden(1);
            fail();
        } catch (InadequateParameterException e) {
            assertEquals(e.getMessage(), "Question 1 don't exist");
        } catch (Exception e){
            fail();
        }
        try{
            Ad a = new Ad();
            a.setTitle("a");
            a.setDescription("a");
            a.setCity("a");
            a.setNeighborhood("a");
            a.setInterestOnBand(true);
            a.setContact(contactPair01);
            a.setInstruments(Arrays.asList(instrSet01));
            a.setNiceStyles(Arrays.asList(niceSty01));
            a.setBadStyles(Arrays.asList(badSty01));
            a.wasAnswered(0);
            fail();
        } catch (InadequateParameterException e) {
            assertEquals(e.getMessage(), "Question 0 don't exist");
        } catch (Exception e){
            fail();
        }
        try{
            Ad a = new Ad();
            a.setTitle("a");
            a.setDescription("a");
            a.setCity("a");
            a.setNeighborhood("a");
            a.setInterestOnBand(true);
            a.setContact(contactPair01);
            a.setInstruments(Arrays.asList(instrSet01));
            a.setNiceStyles(Arrays.asList(niceSty01));
            a.setBadStyles(Arrays.asList(badSty01));
            a.addQuestion(qStr01);
            a.wasAnswered(1);
            fail();
        } catch (InadequateParameterException e) {
            assertEquals(e.getMessage(), "Question 1 don't exist");
        } catch (Exception e){
            fail();
        }
        try{
            Ad a = new Ad();
            a.setTitle("a");
            a.setDescription("a");
            a.setCity("a");
            a.setNeighborhood("a");
            a.setInterestOnBand(true);
            a.setContact(contactPair01);
            a.setInstruments(Arrays.asList(instrSet01));
            a.setNiceStyles(Arrays.asList(niceSty01));
            a.setBadStyles(Arrays.asList(badSty01));
            a.answer(0, aStr01);
            fail();
        } catch (InadequateParameterException e) {
            assertEquals(e.getMessage(), "Question 0 don't exist");
        } catch (Exception e) {
            fail();
        }
        try{
            Ad a = new Ad();
            a.setTitle("a");
            a.setDescription("a");
            a.setCity("a");
            a.setNeighborhood("a");
            a.setInterestOnBand(true);
            a.setContact(contactPair01);
            a.setInstruments(Arrays.asList(instrSet01));
            a.setNiceStyles(Arrays.asList(niceSty01));
            a.setBadStyles(Arrays.asList(badSty01));
            a.addQuestion(qStr01);
            a.answer(1, aStr01);
            fail();
        } catch (InadequateParameterException e) {
            assertEquals(e.getMessage(), "Question 1 don't exist");
        } catch (Exception e) {
            fail();
        }
        try{
            Ad a = new Ad();
            a.setTitle("a");
            a.setDescription("a");
            a.setCity("a");
            a.setNeighborhood("a");
            a.setInterestOnBand(true);
            a.setContact(contactPair01);
            a.setInstruments(Arrays.asList(instrSet01));
            a.setNiceStyles(Arrays.asList(niceSty01));
            a.setBadStyles(Arrays.asList(badSty01));
            a.hide(0);
            fail();
        } catch (InadequateParameterException e) {
            assertEquals(e.getMessage(), "Question 0 don't exist");
        } catch (Exception e){
            fail();
        }
        try{
            Ad a = new Ad();
            a.setTitle("a");
            a.setDescription("a");
            a.setCity("a");
            a.setNeighborhood("a");
            a.setInterestOnBand(true);
            a.setContact(contactPair01);
            a.setInstruments(Arrays.asList(instrSet01));
            a.setNiceStyles(Arrays.asList(niceSty01));
            a.setBadStyles(Arrays.asList(badSty01));
            a.addQuestion(qStr01);
            a.hide(1);
            fail();
        } catch (InadequateParameterException e) {
            assertEquals(e.getMessage(), "Question 1 don't exist");
        } catch (Exception e){
            fail();
        }
    }
    /**
      * Story: Ad bad-styles list creation
      *
      * As a System using Ads
      * I want that when I create Ads, the list of bad-styles given by me can be empty,
      * but cannot be null nor any element can be empty or null.
      * So then I can assure no Errors will be shown displaying this list.
      *
      * Given that an Ad is being created
      * When the list of bad-styles is passed as argument and Ad was correctly created.
      * Then the ad should have a List of Bad-Styles with non-empty elements.
      */
    @Test
    public void shouldHaveCorrectnessOnBadStylesListCreation(){
        try {
            Ad ad = new Ad();
            ad.setTitle("a");
            ad.setDescription("a");
            ad.setCity("a");
            ad.setNeighborhood("a");
            ad.setInterestOnBand(true);
            ad.setContact(contactPair01);
            ad.setInstruments(Arrays.asList(instrSet01));
            ad.setNiceStyles(Arrays.asList(niceSty01));
            ad.setBadStyles(Arrays.asList(badSty01));
            for (int i = 0; i < ad.getNumberOfBadStyles(); i++){
                assertEquals(ad.getBadStyle(i), badSty01[i]);
            }
        }catch (Exception e){
            fail();
        }
        try {
            Ad ad = new Ad();
            ad.setBadStyles(null);
            fail();
        } catch (InadequateParameterException e){
            assertEquals(e.getMessage(), "List of Bad Styles" + " null, or with null or empty values");
        }
        try {
            List<String> a = new ArrayList<>();
            Ad ad = new Ad();
            ad.setBadStyles(a);
        } catch (InadequateParameterException e){
            fail();
        }
        try {
            List<String> a = new ArrayList<>();
            a.add(null);
            Ad ad = new Ad();
            ad.setBadStyles(a);
            fail();
        } catch (InadequateParameterException e){
            assertEquals(e.getMessage(), "List of Bad Styles" + " null, or with null or empty values");
        }
        try {
            List<String> a = new ArrayList<>();
            a.add("");
            Ad ad = new Ad();
            ad.setBadStyles(a);
            fail();
        } catch (InadequateParameterException e){
            assertEquals(e.getMessage(), "List of Bad Styles" + " null, or with null or empty values");
        }
        try {
            List<String> a = new ArrayList<>(Arrays.asList("a", null));
            Ad ad = new Ad();
            ad.setBadStyles(a);
            fail();
        } catch (InadequateParameterException e){
            assertEquals(e.getMessage(), "List of Bad Styles" + " null, or with null or empty values");
        }
        try {
            List<String> a = new ArrayList<>(Arrays.asList("a", ""));
            Ad ad = new Ad();
            ad.setBadStyles(a);
            fail();
        } catch (InadequateParameterException e){
            assertEquals(e.getMessage(), "List of Bad Styles" + " null, or with null or empty values");
        }
        try {
            List<String> a = new ArrayList<>(Arrays.asList(null, "a"));
            Ad ad = new Ad();
            ad.setBadStyles(a);
            fail();
        } catch (InadequateParameterException e){
            assertEquals(e.getMessage(), "List of Bad Styles" + " null, or with null or empty values");
        }
        try {
            List<String> a = new ArrayList<>(Arrays.asList("", "a"));
            Ad ad = new Ad();
            ad.setBadStyles(a);
            fail();
        } catch (InadequateParameterException e){
            assertEquals(e.getMessage(), "List of Bad Styles" + " null, or with null or empty values");
        }
    }
    /**
     * Story: Ad nice-styles list creation
     *
     * As a System using Ads
     * I want that when I create Ads, the list of nice-styles given by me can be empty,
     * but cannot be null nor any element can be empty or null.
     * So then I can assure no Errors will be shown displaying this list.
     *
     * Given that an Ad is being created
     * When the list of nice-styles is passed as argument and Ad was correctly created.
     * Then the ad should have a List of Nice-Styles with non-empty elements.
     */
    @Test
    public void shouldHaveCorrectnessOnNiceStylesListCreation(){
        try {
            Ad ad = new Ad();
            ad.setTitle("a");
            ad.setDescription("a");
            ad.setCity("a");
            ad.setNeighborhood("a");
            ad.setInterestOnBand(true);
            ad.setContact(contactPair01);
            ad.setInstruments(Arrays.asList(instrSet01));
            ad.setNiceStyles(Arrays.asList(niceSty01));
            ad.setBadStyles(Arrays.asList(badSty01));
            for (int i = 0; i < ad.getNumberOfNiceStyles(); i++){
                assertEquals(ad.getNiceStyle(i), niceSty01[i]);
            }
        }catch (Exception e){
            fail();
        }
        try {
            Ad ad = new Ad();
            List<String> a = new ArrayList<>();
            a.add(null);
            ad.setNiceStyles(a);
            fail();
        } catch (InadequateParameterException e){
            assertEquals(e.getMessage(), "List of Nice Styles" + " null, or with null or empty values");
        }
        try {
            List<String> a = new ArrayList<>();
            Ad ad = new Ad();
            ad.setNiceStyles(a);
        } catch (InadequateParameterException e){
            fail();
        }
        try {
            List<String> a = new ArrayList<>();
            a.add(null);
            Ad ad = new Ad();
            ad.setNiceStyles(a);
            fail();
        } catch (InadequateParameterException e){
            assertEquals(e.getMessage(), "List of Nice Styles" + " null, or with null or empty values");
        }
        try {
            List<String> a = new ArrayList<>();
            a.add("");
            Ad ad = new Ad();
            ad.setNiceStyles(a);
            fail();
        } catch (InadequateParameterException e){
            assertEquals(e.getMessage(), "List of Nice Styles" + " null, or with null or empty values");
        }
        try {
            List<String> a = new ArrayList<>(Arrays.asList("a", null));
            Ad ad = new Ad();
            ad.setNiceStyles(a);
            fail();
        } catch (InadequateParameterException e){
            assertEquals(e.getMessage(), "List of Nice Styles" + " null, or with null or empty values");
        }
        try {
            List<String> a = new ArrayList<>(Arrays.asList("a", ""));
            Ad ad = new Ad();
            ad.setNiceStyles(a);
            fail();
        } catch (InadequateParameterException e){
            assertEquals(e.getMessage(), "List of Nice Styles" + " null, or with null or empty values");
        }
        try {
            List<String> a = new ArrayList<>(Arrays.asList(null, "a"));
            Ad ad = new Ad();
            ad.setNiceStyles(a);
            fail();
        } catch (InadequateParameterException e){
            assertEquals(e.getMessage(), "List of Nice Styles" + " null, or with null or empty values");
        }
        try {
            List<String> a = new ArrayList<>(Arrays.asList("", "a"));
            Ad ad = new Ad();
            ad.setNiceStyles(a);
            fail();
        } catch (InadequateParameterException e){
            assertEquals(e.getMessage(), "List of Nice Styles" + " null, or with null or empty values");
        }
    }
    /**
     * Story: Ad instrument list creation
     *
     * As a System using Ads
     * I want that when I create Ads, the list of Instruments given by me will be non-empty
     * with at least one element non-empty.
     * So then I can assure the person displaying the ad knows how to play something.
     *
     * Given that an Ad is being created
     * When the list of Instrument is passed as argument and Ad was correctly created.
     * Then the ad should have a List of Instrument with at least 1 non-empty string.
     */
    @Test
    public void shouldHaveCorrectnessOnInstrumentListCreation(){
        try {
            Ad ad = new Ad();
            ad.setTitle("a");
            ad.setDescription("a");
            ad.setCity("a");
            ad.setNeighborhood("a");
            ad.setInterestOnBand(true);
            ad.setContact(contactPair01);
            ad.setInstruments(Arrays.asList(instrSet01));
            ad.setNiceStyles(Arrays.asList(niceSty01));
            ad.setBadStyles(Arrays.asList(badSty01));
            for (int i = 0; i < ad.getNumberOfInstruments(); i++){
                assertEquals(ad.getInstrument(i), instrSet01[i]);
            }
        }catch (Exception e){
            fail();
        }
        try {
            Ad ad = new Ad();
            ad.setInstruments(null);
            fail();
        } catch (InadequateParameterException e){
            assertEquals(e.getMessage(), "List of Instrument" + " null, empty or with null or empty values");
        }
        try {
            List<String> a = new ArrayList<>();
            Ad ad = new Ad();
            ad.setInstruments(a);
            fail();
        } catch (InadequateParameterException e){
            assertEquals(e.getMessage(), "List of Instrument" + " null, empty or with null or empty values");
        }
        try {
            List<String> a = new ArrayList<>();
            a.add(null);
            Ad ad = new Ad();
            ad.setInstruments(a);
            fail();
        } catch (InadequateParameterException e){
            assertEquals(e.getMessage(), "List of Instrument" + " null, empty or with null or empty values");
        }
        try {
            List<String> a = new ArrayList<>();
            a.add("");
            Ad ad = new Ad();
            ad.setInstruments(a);
            fail();
        } catch (InadequateParameterException e){
            assertEquals(e.getMessage(), "List of Instrument" + " null, empty or with null or empty values");
        }
        try {
            List<String> a = new ArrayList<>(Arrays.asList("a", null));
            Ad ad = new Ad();
            ad.setInstruments(a);
            fail();
        } catch (InadequateParameterException e){
            assertEquals(e.getMessage(), "List of Instrument" + " null, empty or with null or empty values");
        }
        try {
            List<String> a = new ArrayList<>(Arrays.asList("a", ""));
            Ad ad = new Ad();
            ad.setInstruments(a);
            fail();
        } catch (InadequateParameterException e){
            assertEquals(e.getMessage(), "List of Instrument" + " null, empty or with null or empty values");
        }
        try {
            List<String> a = new ArrayList<>(Arrays.asList(null, "a"));
            Ad ad = new Ad();
            ad.setInstruments(a);
            fail();
        } catch (InadequateParameterException e){
            assertEquals(e.getMessage(), "List of Instrument" + " null, empty or with null or empty values");
        }
        try {
            List<String> a = new ArrayList<>(Arrays.asList("", "a"));
            Ad ad = new Ad();
            ad.setInstruments(a);
            fail();
        } catch (InadequateParameterException e){
            assertEquals(e.getMessage(), "List of Instrument" + " null, empty or with null or empty values");
        }
    }
    /**
     * Story: Ad fields creation exceptional behaviour
     *
     * As a System using Ads
     * I want that when I create Ads, giving null or empty values on Simple Fields or giving no form of
     * contact should thrown an Exception.
     * So then I can assure no wrong fields will be created, and detect these errors.
     *
     * Given that an Ad is being created
     * When any Simple field given is null or empty, or when you give no form of contact
     * Then an Exception should be given.
     */
    @Test
    public void shouldThrownExceptionOnBadBehaviourOnCreation() {
        try {
            Ad ad = new Ad();
            ad.setTitle(null);
            fail();
        }catch (InadequateParameterException e){
            assertEquals(e.getMessage(), "Title" + " null or non-given");
        }
        try {
            Ad ad = new Ad();
            ad.setTitle("");
            fail();
        }catch (InadequateParameterException e){
            assertEquals(e.getMessage(), "Title" + " null or non-given");
        }
        try {
            Ad ad = new Ad();
            ad.setDescription(null);
            fail();
        }catch (InadequateParameterException e){
            assertEquals(e.getMessage(), "Description" + " null or non-given");
        }
        try {
            Ad ad = new Ad();
            ad.setDescription("");
            fail();
        }catch (InadequateParameterException e){
            assertEquals(e.getMessage(), "Description" + " null or non-given");
        }
        try {
            Ad ad = new Ad();
            ad.setCity(null);
            fail();
        }catch (InadequateParameterException e){
            assertEquals(e.getMessage(), "City" + " null or non-given");
        }
        try {
            Ad ad = new Ad();
            ad.setCity("");
            fail();
        }catch (InadequateParameterException e){
            assertEquals(e.getMessage(), "City" + " null or non-given");
        }
        try {
            Ad ad = new Ad();
            ad.setNeighborhood(null);
            fail();
        }catch (InadequateParameterException e){
            assertEquals(e.getMessage(), "Neighborhood" + " null or non-given");
        }
        try {
            Ad ad = new Ad();
            ad.setNeighborhood("");
            fail();
        }catch (InadequateParameterException e){
            assertEquals(e.getMessage(), "Neighborhood" + " null or non-given");
        }
        try {
            Ad ad = new Ad();
            ad.setContact(new String[]{null, null});
            fail();
        }catch (InadequateParameterException e){
            assertEquals(e.getMessage(), "No form of contact given.");
        }
        try {
            Ad ad = new Ad();
            ad.setContact(new String[]{null, ""});
            fail();
        }catch (InadequateParameterException e){
            assertEquals(e.getMessage(), "No form of contact given.");
        }
        try {
            Ad ad = new Ad();
            ad.setContact(new String[]{"", null});
            fail();
        }catch (InadequateParameterException e){
            assertEquals(e.getMessage(), "No form of contact given.");
        }
        try {
            Ad ad = new Ad();
            ad.setContact(new String[]{"", ""});
            fail();
        }catch (InadequateParameterException e){
            assertEquals(e.getMessage(), "No form of contact given.");
        }

        try {
            Ad ad = new Ad();
            ad.setContact(null);
            fail();
        }catch (InadequateParameterException e){
            assertEquals(e.getMessage(), "No form of contact given.");
        }
    }
    /**
     * Story: Ad Simple fields creation
     *
     * As a System using Ads
     * I want the Ads to have Title, Description, City, Neighborhood, Code Given,
     * ID(ordered from creation) to be defined on creation of every Ad.
     * So then I can access correctly every one of theses fields if the Ad were created the way I created.
     *
     * Given that an Ad was created
     * When Title, Description, City, Neighborhood, Code Given or ID is consulted
     * Then every one of them should be non-null, non-empty, and equal to parameter given.
     */
    @Test
    public void shouldHaveCorrectnessOnSimpleFieldsAfterCreation() {
        testCorrectnessOnSimpleFieldsAfterCreation("ab", "ac", "ad", "ae");
        testCorrectnessOnSimpleFieldsAfterCreation("bb", "bc", "bd", "be");
    }
    /**
     * Story: Ad Contact pair fields creation
     *
     * As a System using Ads
     * I want the Ads to have one or two contact link defined on creation of every Ad.
     * So then I can access correctly these field if the Ad were created the way I created.
     *
     * Given that an Ad was created
     * When Contact options are consulted
     * Then one or two options will be available and will be equal to parameter given.
     */
    @Test
    public void shouldHaveCorrectnessOnContactAfterCreation() {
        testCorrectnessOnContactAfterCreation(contactPair01);
        testCorrectnessOnContactAfterCreation(contactPair02);
        testCorrectnessOnContactAfterCreation(contactPair03);
    }
    /**
     * Story: Ad Interest on band fields creation
     *
     * As a System using Ads
     * I want the Ads to have Interest on band defined on creation of every Ad.
     * So then I can access correctly these field if the Ad were created the way I created.
     *
     * Given that an Ad was created
     * When Interest on band is consulted
     * Then this field should be equal to parameter given.
     */
    @Test
    public void shouldHaveCorrectnessOnInterestAfterCreation() {
        testCorrectnessOnInterestAfterCreation(true);
        testCorrectnessOnInterestAfterCreation(false);
    }
    /**
     * Story: ID and Code MUST be unique
     *
     * As a System using Ads
     * I want the sucessful creation of Ads to return an Ad with UNIQUE ID and Code
     * So that I can order Ads by Id, and assures the code to be difficult to guess
     *
     * Given that and Ad was correctly created
     * When 2 or more Ads coexist
     * Then each ID and Code must be unique
     */
    @Test
    public void shouldAllIDAndCodeBeUnique(){
        Ad ad01 = null;
        Ad ad02 = null;
        Ad ad03 = null;
        try {
            ad01 = new Ad();
            ad01.setTitle("a");
            ad01.setDescription("b");
            ad01.setCity("c");
            ad01.setNeighborhood("d");
            ad01.setInterestOnBand(true);
            ad01.setContact(contactPair01);
            ad01.setInstruments(Arrays.asList(instrSet01));
            ad01.setNiceStyles(Arrays.asList(niceSty01));
            ad01.setBadStyles(Arrays.asList(badSty01));

            ad02 = new Ad();
            ad02.setTitle("a");
            ad02.setDescription("b");
            ad02.setCity("c");
            ad02.setNeighborhood("d");
            ad02.setInterestOnBand(true);
            ad02.setContact(contactPair01);
            ad02.setInstruments(Arrays.asList(instrSet01));
            ad02.setNiceStyles(Arrays.asList(niceSty01));
            ad02.setBadStyles(Arrays.asList(badSty01));

            ad03 = new Ad();
            ad03.setTitle("f");
            ad03.setDescription("g");
            ad03.setCity("h");
            ad03.setNeighborhood("j");
            ad03.setInterestOnBand(true);
            ad03.setContact(contactPair01);
            ad03.setInstruments(Arrays.asList(instrSet01));
            ad03.setNiceStyles(Arrays.asList(niceSty01));
            ad03.setBadStyles(Arrays.asList(badSty01));
        } catch (InadequateParameterException e) {
            fail();
        }

        testIDAndCodeUniqueBetweenAds(ad01, ad02, ad03);
    }
    /**
     * Story: ID should be ordered by creation time
     *
     * As a System using Ads
     * I want the All Ads to be ordered by creation time, if creation time is the same
     * then it doesn't matter the order, but ID should be unique the same way.
     * So that I can order Ads by Id
     *
     * Given that and Ad was correctly created
     * When Ad A and Ad B exist
     * Then if A was created before, A.id < B.id
     */
    @Test
    public void shouldAllIDBeOrderedByCreationTime(){
        Ad ad01 = null;
        Ad ad02 = null;
        try {
            ad01 = new Ad();
            ad01.setTitle("a");
            ad01.setDescription("b");
            ad01.setCity("c");
            ad01.setNeighborhood("d");
            ad01.setInterestOnBand(true);
            ad01.setContact(contactPair01);
            ad01.setInstruments(Arrays.asList(instrSet01));
            ad01.setNiceStyles(Arrays.asList(niceSty01));
            ad01.setBadStyles(Arrays.asList(badSty01));

            ad02 = new Ad();
            ad02.setTitle("a");
            ad02.setDescription("b");
            ad02.setCity("c");
            ad02.setNeighborhood("d");
            ad02.setInterestOnBand(true);
            ad02.setContact(contactPair01);
            ad02.setInstruments(Arrays.asList(instrSet01));
            ad02.setNiceStyles(Arrays.asList(niceSty01));
            ad02.setBadStyles(Arrays.asList(badSty01));
        } catch (InadequateParameterException e) {
            fail();
        }
        // Wait some time to ensures they were created in different time.
        try {
            Thread.sleep(1);
        }catch (InterruptedException e){
            fail();
        }
        Ad ad03 = null;
        try {
            ad03 = new Ad();
            ad03.setTitle("f");
            ad03.setDescription("g");
            ad03.setCity("h");
            ad03.setNeighborhood("j");
            ad03.setInterestOnBand(true);
            ad03.setContact(contactPair01);
            ad03.setInstruments(Arrays.asList(instrSet01));
            ad03.setNiceStyles(Arrays.asList(niceSty01));
            ad03.setBadStyles(Arrays.asList(badSty01));
        } catch (InadequateParameterException e) {
            fail();
        }
        assertNotEquals(ad01.getID(), ad02.getID());
        assert(ad01.getID() < ad03.getID());
        assert(ad02.getID() < ad03.getID());
    }

    private void testIDAndCodeUniqueBetweenAds(Ad a, Ad b, Ad c){
        testIDUniqueBetweenAds(a, b, c);
        testCodeUniqueBetweenAds(a, b, c);
    }
    private void testCodeUniqueBetweenAds(Ad a, Ad b, Ad c) {
        assertNotEquals(a.getCode(), b.getCode());
        assertNotEquals(a.getCode(), c.getCode());
        assertNotEquals(b.getCode(), c.getCode());
    }
    private void testIDUniqueBetweenAds(Ad a, Ad b, Ad c) {
        assertNotEquals(a.getID(), b.getID());
        assertNotEquals(a.getID(), c.getID());
        assertNotEquals(b.getID(), c.getID());
    }
    private void testCorrectnessOnSimpleFieldsAfterCreation(String t, String d, String c, String n) {
        Ad customAd = null;
        try {
            customAd = new Ad();
            customAd.setTitle(t);
            customAd.setDescription(d);
            customAd.setCity(c);
            customAd.setNeighborhood(n);
            customAd.setInterestOnBand(true);
            customAd.setContact(contactPair01);
            customAd.setInstruments(Arrays.asList(instrSet01));
            customAd.setNiceStyles(Arrays.asList(niceSty01));
            customAd.setBadStyles(Arrays.asList(badSty01));
        } catch (InadequateParameterException e) {
            fail();
        }
        testAllSimpleFieldsWereCreated(customAd);
        testAllSimpleFieldsGivenAreCorrect(customAd, t, d, c, n);
    }
    private void testAllSimpleFieldsWereCreated(Ad ad) {
        try{
            assertTrue(!ad.getTitle().equals(""));
            assertTrue(!ad.getDescription().equals(""));
            assertTrue(!ad.getCity().equals(""));
            assertTrue(!ad.getNeighborhood().equals(""));
            assertTrue(!ad.getCode().equals(""));
            assertTrue(!(ad.getID() <= 0));
        }catch (Exception e){
            fail();
        }
    }
    private void testAllSimpleFieldsGivenAreCorrect(Ad ad, String t, String d, String c, String n) {
        assertTrue(ad.getTitle().equals(t));
        assertTrue(ad.getDescription().equals(d));
        assertTrue(ad.getCity().equals(c));
        assertTrue(ad.getNeighborhood().equals(n));
    }
    private void testCorrectnessOnContactAfterCreation(String [] ctcPair){
        assertEquals(ctcPair.length, 2);
        Ad customAd = null;
        try {
            customAd = new Ad();
            customAd.setTitle("a");
            customAd.setDescription("b");
            customAd.setCity("c");
            customAd.setNeighborhood("d");
            customAd.setInterestOnBand(true);
            customAd.setContact(ctcPair);
            customAd.setInstruments(Arrays.asList(instrSet01));
            customAd.setNiceStyles(Arrays.asList(niceSty01));
            customAd.setBadStyles(Arrays.asList(badSty01));
        } catch (InadequateParameterException e) {
            fail();
        }
        if(customAd.hasFacebookContact()){
            assertEquals(ctcPair[0], customAd.getFacebookContact());
        }else{
            assertEquals("", customAd.getFacebookContact());
        }
        if (customAd.hasEmailContact()){
            assertEquals(ctcPair[1], customAd.getEmailContact());
        }else{
            assertEquals("", customAd.getEmailContact());
        }
    }
    private void testCorrectnessOnInterestAfterCreation(boolean i) {
        Ad customAd = null;
        try {
            customAd = new Ad();
            customAd.setTitle("a");
            customAd.setDescription("b");
            customAd.setCity("c");
            customAd.setNeighborhood("d");
            customAd.setInterestOnBand(i);
            customAd.setContact(contactPair01);
            customAd.setInstruments(Arrays.asList(instrSet01));
            customAd.setNiceStyles(Arrays.asList(niceSty01));
            customAd.setBadStyles(Arrays.asList(badSty01));
        } catch (InadequateParameterException e) {
            fail();
        }
        assertEquals(customAd.isInterestOnBand(), i);
    }
}


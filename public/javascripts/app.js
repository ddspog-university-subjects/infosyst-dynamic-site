var app = angular.module('GigCreatorApp', ['ngRoute'])
    .config(["$routeProvider", function($routeProvider){
        return $routeProvider
            .when("/", {
                templateUrl: "/views/all",
                controller: "ViewsController"
            })
            .when("/all/:id", {
                templateUrl: function(urlattr){
                    return "/views/all?id=" + urlattr.id;
                },
                controller: "ViewsController"
            })
            .when("/search/:tag/:option/:id", {
                templateUrl: function (urlattr) {
                    return "/views/search" +
                        "?tag=" + urlattr.tag +
                        "&option=" + urlattr.option +
                        "&id=" + urlattr.id;
                },
                controller: "ViewsController"
            })
            .otherwise({
                redirectTo: "/"
            });

    }])
    .config(["$locationProvider", function($locationProvider){
        return $locationProvider.html5Mode(true).hashPrefix("!");
    }]);


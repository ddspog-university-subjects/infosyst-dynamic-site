app.controller('CreateAdController', function($scope, $http, $window) {
    $scope.createAd = function(){
        var formData = $scope.fields;

        $http({
            method: 'POST',
            url: '/tasks/create',
            data: formData
        })
            .success(function (response) {
                $window.location.reload();
            })
    };
});
app.controller('GeneralController', function($scope, $http) {
    $http.get('/api/text.json')
    .then(function successCallback(response){
        $scope.sidebarText = response.data.sidebarText;
        $scope.searchBarText = response.data.searchBarText;
        $scope.headerText = response.data.headerText;
        $scope.hiddenText = response.data.hiddenText;
        $scope.btnDeleteText = response.data.btnDeleteText;
        $scope.btnCreateAd = response.data.btnCreateAd;
        $scope.modalCreateAd = response.data.modalCreateAd;
        $scope.adsSectionTitleText = response.data.adsSectionTitleText;
    },    function errorCallback(response){
        $scope.sidebarText = {};
        $scope.searchBarText = {};
        $scope.headerText = {};
        $scope.hiddenText = {};
        $scope.btnDeleteText = {};
        $scope.btnCreateAd = {};
        $scope.modalCreateAd = {};
        $scope.adsSectionTitleText = {};
        });
});
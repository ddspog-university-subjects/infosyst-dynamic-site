app.directive('header', function(){
    return {
        restrict: 'EA',
        scope: {
            item: '='
        },
        templateUrl: '/assets/javascripts/directives/header/header.html'
    };
});
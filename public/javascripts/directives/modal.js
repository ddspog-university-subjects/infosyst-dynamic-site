app.directive('btnModal', function(){
    return {
        restrict: 'EA',
        transclude: true,
        scope: {
            item: '='
        },
        templateUrl: '/assets/javascripts/directives/modal/btnModal.html'
    };
});

app.directive('modalCreate', function(){
    return {
        restrict: 'EA',
        scope: {
            item: '='
        },
        templateUrl: '/assets/javascripts/directives/modal/modalCreate.html',
        controller: 'CreateAdController'
    };
});

app.directive('ngPlaceholder', function () {
    return {
        restrict: 'A',
        scope: {
            placeholder: '=ngPlaceholder'
        },
        link: function(scope, elem, attr) {
            scope.$watch('placeholder',function() {
                elem[0].placeholder = scope.placeholder;
            });
        }
    }
});
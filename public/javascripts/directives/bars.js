app.directive('sideBar', function(){
    return {
        restrict: 'EA',
        scope: {
            item: '='
        },
        templateUrl: '/assets/javascripts/directives/bars/sideBar.html',
        link: function(scope, element, attrs){
            scope.toggleMenu = function(){
                $('#theMenu').toggleClass('menu-open');
            }
        }
    };
});

app.directive('searchBar', function(){
    return {
        restrict: 'EA',
        scope: {
            item: '='
        },
        templateUrl: '/assets/javascripts/directives/bars/searchBar.html'
    };
});
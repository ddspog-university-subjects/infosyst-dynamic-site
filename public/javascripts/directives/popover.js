app.directive('btnDelete', function(){
    return {
        restrict: 'EA',
        scope: false,
        templateUrl: '/assets/javascripts/directives/popover/btnDelete.html',
        link: function(scope, element, attrs){
            $('.spawn-popover-delete-ad').popover({
                html : true,
                content: function() {
                    $('.popover').remove();
                    return $('#popover-delete-ad-content').html();
                },
                title: function(){
                    return scope.btnDeleteText.title;
                }
            });
        }
    };
});

app.directive('hiddenPopover', function(){
    return {
        restrict: 'EA',
        scope: {
            item: '='
        },
        templateUrl: '/assets/javascripts/directives/popover/hiddenPopover.html'
    };
});
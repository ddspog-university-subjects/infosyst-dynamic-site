app.directive('sectionTitle', function(){
    return {
        restrict: 'EA',
        transclude: true,
        scope: {
            item: '='
        },
        templateUrl: '/assets/javascripts/directives/sections/sectionTitle.html'
    };
});

app.directive('sectionAds', function(){
    return {
        restrict: 'EA',
        transclude: true,
        scope: {
            item: '='
        },
        templateUrl: '/assets/javascripts/directives/sections/sectionAds.html'
    };
});
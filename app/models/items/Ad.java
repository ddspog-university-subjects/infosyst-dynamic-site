package models.items;

import models.exceptions.InadequateParameterException;
import models.exceptions.UnauthorizedOverwriteException;

import java.security.SecureRandom;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;

/**
 * Class defining Ad structure and behavior when creating or acessing them.
 */
public class Ad {

    private String title;
    private String description;
    private String neighborhood;
    private String city;
    private String code;
    private long id;
    private boolean interestOnBand;
    private String[] contact;

    private SecureRandom random = new SecureRandom();
    private Predicate<String> correctValue = (String s) -> s != null && !s.isEmpty();
    private Predicate<Integer> outOfQuestionBounds = (Integer i) -> i < 0 || i >= getNumberOfQuestions();

    private static final int MAX_CREATION_PER_SECOND = 10000;
    private List<String> instruments;
    private List<String> niceStyles;
    private List<String> badStyles;
    private List<Question> questions;

    public Ad(){
        createCode();
        createID();
        initializeLists();
    }

    private void initializeLists() {
        this.instruments = new ArrayList<>();
        this.niceStyles= new ArrayList<>();
        this.badStyles = new ArrayList<>();
        this.questions = new ArrayList<>();
    }

    private void setQuestions() {
        this.questions = new ArrayList<>();
    }

    public String getTitle(){
        return this.title;
    }

    public void setTitle(String t) throws InadequateParameterException {
        if(!correctValue.test(t))
            throw new InadequateParameterException("Title null or non-given");
        this.title = t;
    }

    public String getDescription(){
        return this.description;
    }

    public void setDescription(String d) throws InadequateParameterException {
        if(!correctValue.test(d))
            throw new InadequateParameterException("Description null or non-given");
        this.description = d;
    }

    public String getCity(){
        return this.city;
    }

    public void setCity(String c) throws InadequateParameterException {
        if(!correctValue.test(c))
            throw new InadequateParameterException("City null or non-given");
        this.city = c;
    }

    public String getNeighborhood(){
        return this.neighborhood;
    }

    public void setNeighborhood(String n) throws InadequateParameterException {
        if(!correctValue.test(n))
            throw new InadequateParameterException("Neighborhood null or non-given");
        this.neighborhood = n;
    }

    public String getCode(){
        return this.code;
    }

    public void setCode(String code){
        this.code = code;
    }

    private void createCode(){
        this.code = (new BigInteger(130, random)).toString(32);
    }

    public long getID(){
        return this.id;
    }

    public void setID(long id){
        this.id = id;
    }

    private void createID(){
        this.id = getUniqueTimeValue();
    }

    private long getUniqueTimeValue(){
        long now = System.currentTimeMillis() * MAX_CREATION_PER_SECOND;
        now += random.nextInt() % MAX_CREATION_PER_SECOND;
        return now;
    }

    public boolean isInterestOnBand(){
        return this.interestOnBand;
    }

    public void setInterestOnBand(boolean i) {
        this.interestOnBand = i;
    }

    public boolean hasFacebookContact() {
        return !this.contact[0].isEmpty();
    }

    public String getFacebookContact() {
        return this.contact[0];
    }

    public boolean hasEmailContact() {
        return !this.contact[1].isEmpty();
    }

    public String getEmailContact() {
        return this.contact[1];
    }

    public void setContact(String[] c) throws InadequateParameterException {
        this.contact = new String[2];
        if(c == null || (!correctValue.test(c[0]) && !correctValue.test(c[1]))) {
            throw new InadequateParameterException("No form of contact given.");
        }
        this.contact[0] = (correctValue.test(c[0])) ? c[0] : "";
        this.contact[1] = (correctValue.test(c[1])) ? c[1] : "";

    }

    public static String borderString = "=====";
    public static String borderQuestion = ";;;;;";

    public static String convertListToString(List<?> l){
        String toReturn = "";
        if(l ==  null)
            return toReturn;
        for (Object o : l) {
            if(o instanceof Question) {
                Question q = (Question) o;
                toReturn += q.getQuestion() + borderQuestion + q.getAnswer() + borderString;
            } else {
                String s = (String) o;
                toReturn += s + borderString;
            }
        }
        if(toReturn.isEmpty())
            return toReturn;
        else
            return toReturn.substring(0, toReturn.length() - 5);
    }

    public static List<String> convertStringToList(String s){
        if(s.equals((""))){
            return new ArrayList<>();
        }else{
            return Arrays.asList(s.split(borderString));
        }
    }

    public int getNumberOfInstruments() {
        return this.instruments.size();
    }

    public String getInstrument(int i) {
        return this.instruments.get(i);

    }

    public String getInstruments(){
        return convertListToString(this.instruments);
    }

    public void setInstruments(List<String> i) throws InadequateParameterException {
        if(i == null || i.isEmpty() || !i.stream().allMatch(correctValue)){
            throw new InadequateParameterException("List of Instrument null, empty or with null or empty values");
        }
        this.instruments = i;
    }

    public int getNumberOfNiceStyles() {
        return this.niceStyles.size();
    }

    public String getNiceStyle(int i) {
        return this.niceStyles.get(i);
    }

    public String getNiceStyles(){
        return convertListToString(this.niceStyles);
    }

    public void setNiceStyles(List<String> niceStyles) throws InadequateParameterException {
        if(niceStyles == null || !niceStyles.stream().allMatch(correctValue)) {
            throw new InadequateParameterException("List of Nice Styles null, or with null or empty values");
        }
        this.niceStyles = niceStyles;
    }

    public int getNumberOfBadStyles() {
        return this.badStyles.size();
    }

    public String getBadStyle(int i) {
        return this.badStyles.get(i);
    }

    public String getBadStyles(){
        return convertListToString(this.badStyles);
    }

    public void setBadStyles(List<String> badStyles) throws InadequateParameterException {
        if(badStyles == null || !badStyles.stream().allMatch(correctValue)) {
            throw new InadequateParameterException("List of Bad Styles null, or with null or empty values");
        }
        this.badStyles = badStyles;
    }

    public String getQuestion(int i) throws InadequateParameterException {
        if (outOfQuestionBounds.test(i))
            throw new InadequateParameterException("Question " + i + " don't exist");
        return this.questions.get(i).getQuestion();
    }

    public String getQuestions(){
        return convertListToString(this.questions);
    }

    public int getNumberOfQuestions() {
        return this.questions.size();
    }

    public void addQuestion(String qStr) throws InadequateParameterException {
        Question q = new Question(qStr);
        this.questions.add(q);
    }

    public boolean wasAnswered(int i) throws InadequateParameterException {
        if (outOfQuestionBounds.test(i))
            throw new InadequateParameterException("Question " + i + " don't exist");
        return this.questions.get(i).wasAnswered();
    }

    public String getAnswer(int i) throws InadequateParameterException {
        if (outOfQuestionBounds.test(i))
            throw new InadequateParameterException("Question " + i + " don't exist");
        return this.questions.get(i).getAnswer();
    }

    public void answer(int i, String ans) throws UnauthorizedOverwriteException, InadequateParameterException {
        if (outOfQuestionBounds.test(i))
            throw new InadequateParameterException("Question " + i + " don't exist");
        this.questions.get(i).answer(ans);
    }

    public boolean isHidden(int i) throws InadequateParameterException {
        if (outOfQuestionBounds.test(i))
            throw new InadequateParameterException("Question " + i + " don't exist");
        return this.questions.get(i).isHidden();
    }

    public void hide(int i) throws InadequateParameterException {
        if (outOfQuestionBounds.test(i))
            throw new InadequateParameterException("Question " + i + " don't exist");
        this.questions.get(i).hide();
    }

    @Override
    public boolean equals(Object obj) {
        if(obj instanceof Ad){
            if (this.getID() == ((Ad)obj).getID())
                return true;
        }
        return false;
    }
}
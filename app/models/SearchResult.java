package models;

import models.exceptions.InadequateParameterException;
import models.items.Ad;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

/**
 * Made to do operations of search in List of Ads.
 */
public class SearchResult {

    private enum SearchMethod {
        VIA_KEYWORD, VIA_INSTRUMENT, VIA_NICE_STYLE, VIA_INTEREST
    }

    private List<Ad> list;
    private List<Integer> indexes;
    private Predicate<String> correctValue = (String s) -> s != null && !s.isEmpty();
    private Predicate<Integer> outOfBounds = (Integer i) -> i < 0 || i >= size();


    public SearchResult(List<Ad> mainList) throws InadequateParameterException {
        setList(mainList);
    }

    public int size() {
        return this.indexes.size();
    }

    public Ad get(int i) throws InadequateParameterException {
        if(outOfBounds.test(i)){
            throw new InadequateParameterException("Index out of scope");
        }
        return this.list.get(this.indexes.get(i));
    }

    public SearchMethod viaKeyword(){
        return SearchMethod.VIA_KEYWORD;
    }
    public SearchMethod viaInstrument(){
        return SearchMethod.VIA_INSTRUMENT;
    }
    public SearchMethod viaNiceStyle(){
        return SearchMethod.VIA_NICE_STYLE;
    }
    public SearchMethod viaInterest(){
        return SearchMethod.VIA_INTEREST;
    }

    private void setList(List<Ad> list) throws InadequateParameterException {
        if (list == null || !list.stream().allMatch((a) -> a != null)){
            throw new InadequateParameterException("List given should be non-null with non-null values");
        }
        this.list = list;
        this.indexes = new ArrayList<>();
        for (int i = 0; i < this.list.size(); i++) {
            this.indexes.add(i);
        }
    }

    public void search(SearchMethod searchMethod, String searchString) throws Exception {
        if(!correctValue.test(searchString)){
            throw new InadequateParameterException("Search string should be non-empty");
        }
        searchString = searchString.toLowerCase();
        switch (searchMethod){
            case VIA_KEYWORD:
                searchViaKeyword(searchString);
                break;
            case VIA_INSTRUMENT:
                searchViaInstrument(searchString);
                break;
            case VIA_INTEREST:
                searchViaInterest(searchString);
                break;
            case VIA_NICE_STYLE:
                searchViaNiceStyle(searchString);
                break;
            default:
                break;
        }
        if (indexes.size() == 0)
            throw new Exception("Search has encounter no result");
    }

    private void searchViaKeyword(String searchString) {
        this.indexes = new ArrayList<>();
        for (int i = 0; i < list.size(); i++) {
            boolean wasNotAdded = true;
            if(list.get(i).getTitle().toLowerCase().contains(searchString)){
                this.indexes.add(i);
                wasNotAdded = false;
            }
            if(wasNotAdded && list.get(i).getDescription().toLowerCase().contains(searchString)){
                this.indexes.add(i);
                wasNotAdded = false;
            }
            if(wasNotAdded && list.get(i).getCity().toLowerCase().contains(searchString)){
                this.indexes.add(i);
                wasNotAdded = false;
            }
            if(wasNotAdded && list.get(i).getNeighborhood().toLowerCase().contains(searchString)){
                this.indexes.add(i);
                wasNotAdded = false;
            }
            for (int j = 0; wasNotAdded && j < list.get(i).getNumberOfInstruments(); j++) {
                if(list.get(i).getInstrument(j).toLowerCase().contains(searchString)) {
                    this.indexes.add(i);
                    wasNotAdded = false;
                }
            }
            for (int j = 0; wasNotAdded && j < list.get(i).getNumberOfNiceStyles(); j++) {
                if(list.get(i).getNiceStyle(j).toLowerCase().contains(searchString)) {
                    this.indexes.add(i);
                    wasNotAdded = false;
                }
            }
            for (int j = 0; wasNotAdded && j < list.get(i).getNumberOfBadStyles(); j++) {
                if(list.get(i).getBadStyle(j).toLowerCase().contains(searchString)) {
                    this.indexes.add(i);
                    wasNotAdded = false;
                }
            }
        }
    }

    private void searchViaNiceStyle(String searchString) {
        this.indexes = new ArrayList<>();
        for (int i = 0; i < list.size(); i++) {
            boolean wasNotAdded = true;
            for (int j = 0; wasNotAdded && j < list.get(i).getNumberOfNiceStyles(); j++) {
                if(list.get(i).getNiceStyle(j).toLowerCase().equals(searchString)) {
                    this.indexes.add(i);
                    wasNotAdded = false;
                }
            }
        }
    }

    private void searchViaInstrument(String searchString) {
        this.indexes = new ArrayList<>();
        for (int i = 0; i < list.size(); i++) {
            boolean wasNotAdded = true;
            for (int j = 0; wasNotAdded && j < list.get(i).getNumberOfInstruments(); j++) {
                if(list.get(i).getInstrument(j).toLowerCase().equals(searchString)) {
                    this.indexes.add(i);
                    wasNotAdded = false;
                }
            }
        }
    }

    private void searchViaInterest(String searchString) {
        this.indexes = new ArrayList<>();
        for (int i = 0; i < list.size(); i++) {
            if(searchString.contains("banda"))
                if(list.get(i).isInterestOnBand())
                    this.indexes.add(i);
            if(searchString.contains("so"))
                if(!list.get(i).isInterestOnBand())
                    this.indexes.add(i);
        }
    }
}

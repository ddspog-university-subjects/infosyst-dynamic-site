package models.dao;

import controllers.ConnectionFactory;
import models.exceptions.InadequateParameterException;
import models.items.Ad;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by WIN7 on 25/10/2015.
 */
public class AdDao {

    private Connection connection;
    private static String adInsertionPattern = "insert into ads" +
            " (id,title,description,city,neighborhood,facebook,email,interest,instruments,niceStyles,badStyles,questions,code)" +
            " values (?,?,?,?,?,?,?,?,?,?,?,?,?)";
    private static String adDeletionPattern = "delete from ads where id=?";
    private static String adSearchAll = "select * from ads";
    private static String adSearchWhereIDIs = "select * from ads where id=?";

    public AdDao(){
        startConnection();
    }

    private void startConnection() {
        connection = new ConnectionFactory().getConnection();
    }

    public void add(Ad ad){
        try{
            executeInsertStatement(connection, ad);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    private void executeInsertStatement(Connection con, Ad ad) throws SQLException {
        PreparedStatement stmt;
        stmt = con.prepareStatement(adInsertionPattern);
        // Fill values
        stmt.setLong(1, ad.getID());
        stmt.setString(2, ad.getTitle());
        stmt.setString(3, ad.getDescription());
        stmt.setString(4, ad.getCity());
        stmt.setString(5, ad.getNeighborhood());
        stmt.setString(6, ad.getFacebookContact());
        stmt.setString(7, ad.getEmailContact());
        stmt.setBoolean(8, ad.isInterestOnBand());
        stmt.setString(9, ad.getInstruments());
        stmt.setString(10, ad.getNiceStyles());
        stmt.setString(11, ad.getBadStyles());
        stmt.setString(12, ad.getQuestions());
        stmt.setString(13, ad.getCode());
        // Execute
        stmt.execute();
        stmt.close();
    }

    public void remove(Ad ad){
        try{
            executeDeleteStatement(connection, ad);
        } catch (SQLException e){
            throw new RuntimeException(e);
        }
    }

    public void executeDeleteStatement(Connection con, Ad ad) throws SQLException {
        PreparedStatement stmt;
        stmt = con.prepareStatement(adDeletionPattern);
        // Fill values
        stmt.setLong(1, ad.getID());
        // Execute
        stmt.execute();
        stmt.close();
    }

    public Ad getAdWithID(long id){
        try {
            return executeSelectWhereIDIsStatement(id);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } catch (InadequateParameterException e) {
            throw new RuntimeException("Problem retrieving data from database. Object in database was created with" +
                    " invalid parameters.");
        }
    }

    private Ad executeSelectWhereIDIsStatement(long id) throws SQLException, InadequateParameterException {
        List<Ad> ads = new ArrayList<>();
        PreparedStatement stmt = connection.prepareStatement(adSearchWhereIDIs);
        stmt.setLong(1, id);

        ResultSet rs = stmt.executeQuery();
        Ad ad = null;
        while (rs.next()){
            // Creating object Ad
            ad = new Ad();
            ad.setID(rs.getLong("id"));
            ad.setCode(rs.getString("code"));
            ad.setTitle(rs.getString("title"));
            ad.setDescription(rs.getString("description"));
            ad.setCity(rs.getString("city"));
            ad.setNeighborhood(rs.getString("neighborhood"));
            ad.setInterestOnBand(rs.getBoolean("interest"));
            ad.setInstruments(Ad.convertStringToList(rs.getString("instruments")));
            ad.setNiceStyles(Ad.convertStringToList(rs.getString("niceStyles")));
            ad.setBadStyles(Ad.convertStringToList(rs.getString("badStyles")));
        }
        rs.close();
        stmt.close();
        return ad;
    }

    public List<Ad> getList(){
        try {
            return executeSelectStatement();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } catch (InadequateParameterException e) {
            throw new RuntimeException("Problem retrieving data from database. Object in database was created with" +
                    " invalid parameters.");
        }
    }

    private List<Ad> executeSelectStatement() throws SQLException, InadequateParameterException {
        List<Ad> ads = new ArrayList<>();
        PreparedStatement stmt = connection.prepareStatement(adSearchAll);
        ResultSet rs = stmt.executeQuery();

        while (rs.next()){
            // Creating object Ad
            Ad ad = new Ad();
            ad.setID(rs.getLong("id"));
            ad.setCode(rs.getString("code"));
            ad.setTitle(rs.getString("title"));
            ad.setDescription(rs.getString("description"));
            ad.setCity(rs.getString("city"));
            ad.setNeighborhood(rs.getString("neighborhood"));
            ad.setInterestOnBand(rs.getBoolean("interest"));
            ad.setContact(new String[]{rs.getString("facebook"), rs.getString("email")});
            ad.setInstruments(Ad.convertStringToList(rs.getString("instruments")));
            ad.setNiceStyles(Ad.convertStringToList(rs.getString("niceStyles")));
            ad.setBadStyles(Ad.convertStringToList(rs.getString("badStyles")));

            ads.add(ad);
        }
        rs.close();
        stmt.close();
        return ads;
    }
}

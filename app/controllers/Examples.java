package controllers;

import models.items.Ad;
import models.exceptions.InadequateParameterException;

import java.util.*;
import java.util.function.Supplier;

/**
 * Created by dennis on 09/06/15.
 */
public class Examples {

    private static final String [] titl = new String[]{"Violeira pronta pra tocar", "João quer cantar", "Mística " +
            "libriana quer assombrar", "Vamos montar um clube de violinos?", "Gabriela cor do pecado", "Preciso de uma " +
            "banda pra MPB", "Manuela Pimenta", "Procura-se parceiro pra dupla sertaneja", "Preciso de uma galera pra " +
            "tocar num festival", "Guilherme Melo, Baterista"};
    private static final String [] desc = new String[]{"Sou Amanda, vim do interior de Goiás e procuro oportunidades" +
            " para tocar e alegrar nos bares da região", "Sou idoso já, mas ainda tenho voz. Quero compartilhar de minha" +
            " experiência dos anos 50 de cantoria para todos os jovens de hoje", "26 anos de idade, nome Laura Arroz, " +
            "quero cantar os rocks da vida, dizer que tipo de droga tá na moda pra os jovens e etc.", "Me chamo Sophia e " +
            "já tenho quase 80 anos, porém sei tocar Piano como poucos. Entretanto, andei tendo muitas decepções na minha " +
            "vida para continuar com carreira ou algo assim, gostaria de fazer diferente. Quero juntar um grupo de " +
            "violinistas para tocarmos alguns clássicos de vez em quando.", "Quero uma banda MUITO indecente, para " +
            "satisfazermos os desejos de muitos.", "Sou Danilo, canto e toco violão, quero juntar uma galera legal para " +
            "fazer uns shows massa no centro da cidade, pra tocar no canto de um amigo meu, pago bem. E também posso " +
            "ajudar no custeio de instrumentos e etc.", "Procurando nova banda para participar, tenho experiência e " +
            "alguns produtores interessados em BOA QUALIDADE de músicos para compor um grande estouro nas rádios daqui.",
            "Sou Bruno, galã de 57 anos, e com uma experiência de palco de 20 anos. Meu parceiro original faleceu de câncer " +
            "no pulmão, e depois de anos do choque, eu e meus antigos produtores decidimos que era hora de voltar ao " +
            "palco. Então agora procuro mais um novo colega de qualquer idade para a experiência tão prazerosa que é " +
            "cantar sertanejo para o mundo ouvir", "Olá gente, sou Emilly e sou professora do curso de música na UFRJ. " +
            "Recentemente uma amiga minha me veio pedir para tocar com minha banda na escola que ela é diretora. Mas " +
            "minha banda no mesmo dia já está ocupada pra tocar em outro lugar. Eles podem tocar sem mim, mas ainda " +
            "precisarei de um Baterista e de alguém no Teclado para o próximo dia 29 desse mês.", "Gente, tenho 60 anos " +
            "e quero tocar Jazz semanalmente, quem Bora?"};
    private static final String [] city = {"Guarulhos", "Tucuruí", "Bauru", "Salvador", "Itaúna", "Carapicuíba", "Anápolis",
            "Cascavel", "Rio de Janeiro", "Juiz de Fora"};
    private static final String [] neig = {"São Miguel Aleixo", "Presidente Médici", "Marinho", "Pedra Grande",
            "Domingos Rodrigues Silva", "Benedita Modesto", "Bairro 9", "Juruna", "Rio das Tintas", "Padre Júlio Maria"};
    private static final String[][] cont = {new String[]{"Wiche1983", "AmandaAzevedoCavalcanti@teleworm.us"},
            new String[]{"Busly1935", "JoaoPintoCosta@jourrapide.com"}, new String[]{"eeyohk9SeB",
            "LauraGoncalvesAzevedo@dayrep.com"}, new String[]{"vovo.sophia.dopiano", ""}, new String[]{"",
            "GabrielaFerreiraGoncalves@teleworm.us"}, new String[]{"marconi.santos95", "DaniloFerreiraPereira@jourrapide.com"},
            new String[]{"", "ManuelaAlvesFerreira@armyspy.com"}, new String[]{"", "BrunoCarvalhoCastro@jourrapide.com"},
            new String[]{"emillycastro.costa2008", ""}, new String[]{"vovoguilherme.jazz", ""}};
    private static final List[] inst = new List[]{Arrays.asList("Violão", "Viola", "Cavaquinho"),
            Arrays.asList("Violão", "Piano", "Pandeiro"), Collections.singletonList("Minha boca"), Arrays.asList("Piano",
            "Violino", "Violoncelo"), Arrays.asList("Guitarra", "Baixo", "Canto"), Arrays.asList("Violão", "Viola"),
            Arrays.asList("Guitarra", "Violão", "Violino", "Violoncelo"), Collections.singletonList("Violão"),
            Collections.singletonList("Guitarra"), Collections.singletonList("Bateria")};
    private static final List[] nSty = new List[]{Arrays.asList("Sertanejo", "Samba de raiz", "Pagode", "Cantoria"),
            Arrays.asList("Brega", "Sertanejo", "MPB"), new ArrayList<>(), Arrays.asList("Tango", "Clássico"),
            Arrays.asList("Death Metal", "Black Metal", "Trash Metal", "Metal"), Collections.singletonList("MPB"),
            Arrays.asList("Rock Clássico", "Rock Psicodélico"), new ArrayList<>(), new ArrayList<>(), new ArrayList<>()};
    private static final List[] bSty = new List[]{Arrays.asList("Death Metal", "Forró Universitário", "Sertanejo Universitário"),
            new ArrayList<>(), new ArrayList<>(), new ArrayList<>(), Arrays.asList("Forró", "Samba", "Funk"), new ArrayList<>(),
            new ArrayList<>(), new ArrayList<>(), new ArrayList<>(), new ArrayList<>()};
    private static Random rand = new Random();
    private static Supplier<Integer> rind = () -> {
            int x = rand.nextInt();
            return (x > 0) ? (x % 10) : ((x*(-1)) % 10);
    };

    public static Ad getSecureAd(String t, String d, String c, String n, boolean i, String[] ac, List<String> li, List<String> ln, List<String> lb){
        try{
            Ad ad = new Ad();
            ad.setTitle(t);
            ad.setDescription(d);
            ad.setCity(c);
            ad.setNeighborhood(n);
            ad.setInterestOnBand(i);
            ad.setContact(ac);
            ad.setInstruments(li);
            ad.setNiceStyles(ln);
            ad.setBadStyles(lb);
            return ad;
        } catch (InadequateParameterException e){
            throw new RuntimeException("Problem with my Ad parameters on controllers.Examples.");
        }
    }

    public static Ad getAdGuilhermeFromJuizDeFora() {
        return getSecureAd(titl[9], desc[9], city[9], neig[9], false, cont[9], inst[9], nSty[9], bSty[9]);}
    public static Ad getAdEmillyFromRioDeJaneiro() {
        return getSecureAd(titl[8], desc[8], city[8], neig[8], false, cont[8], inst[8], nSty[8], bSty[8]);}
    public static Ad getAdBrunoFromCascavel() {
        return getSecureAd(titl[7], desc[7], city[7], neig[7], true, cont[7], inst[7], nSty[7], bSty[7]);}
    public static Ad getAdManuelaFromAnapolis() {
        return getSecureAd(titl[6], desc[6], city[6], neig[6], true, cont[6], inst[6], nSty[6], bSty[6]);}
    public static Ad getAdDaniloFromCarapicuiba() {
        return getSecureAd(titl[5], desc[5], city[5], neig[5], true, cont[5], inst[5], nSty[5], bSty[5]);}
    public static Ad getAdGabrielaFromItauna() {
        return getSecureAd(titl[4], desc[4], city[4], neig[4], true, cont[4], inst[4], nSty[4], bSty[4]);}
    public static Ad getAdSophiaFromSalvador() {
        return getSecureAd(titl[3], desc[3], city[3], neig[3], false, cont[3], inst[3], nSty[3], bSty[3]);}
    public static Ad getAdLauraFromBauru() {
        return getSecureAd(titl[2], desc[2], city[2], neig[2], true, cont[2], inst[2], nSty[2], bSty[2]);}
    public static Ad getAdJoaoFromTucurui() {
        return getSecureAd(titl[1], desc[1], city[1], neig[1], true, cont[1], inst[1], nSty[1], bSty[1]);}
    public static Ad getAdAmandaFromGuarulhos() {
        return getSecureAd(titl[0], desc[0], city[0], neig[0], false, cont[0], inst[0], nSty[0], bSty[0]);}

    public static Ad getAdRandom() {
        return getSecureAd(getTitleRandom(), getDescriptionRandom(), getCityRandom(), getNeighborhoodRandom(),
               getInterestRandom(), getAccountsRandom(), getInstrumentsRandom(), getNiceStylesRandom(), getBadStylesRandom());
    }

    public static List getBadStylesRandom() {
        return bSty[rind.get()];
    }

    public static List getNiceStylesRandom() {
        return nSty[rind.get()];
    }

    public static List getInstrumentsRandom() {
        return inst[rind.get()];
    }

    public static String[] getAccountsRandom() {
        return cont[rind.get()];
    }

    public static boolean getInterestRandom() {
        return rand.nextBoolean();
    }

    public static String getTitleRandom(){
        return titl[rind.get()];
    }

    public static String getDescriptionRandom(){
        return desc[rind.get()];
    }

    public static String getCityRandom(){
        return city[rind.get()];
    }

    public static String getNeighborhoodRandom(){
        return neig[rind.get()];
    }

}

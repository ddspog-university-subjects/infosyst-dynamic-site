package controllers;

/**
 * Created by dennis on 09/06/15.
 */
public class Utils {

    public static int getNumberOfPages() {
        if(Application.getNumberOfAds() % 9 == 0){
            return Application.getNumberOfAds() / 9;
        }else{
            return Application.getNumberOfAds() / 9 + 1;
        }
    }

    public static int getNextPage(int i) {
        return i + 1;
    }
    public static int getPreviousPage(int i) { return i - 1;  }

    public static boolean isTheLastPage(int i){
        return i == getNumberOfPages() - 1;
    }

    public static boolean isTheFirstPage(int i) {return i == 0; }

    public static boolean isAllInstance(String s) { return s.equals("all"); }

    public static int getNumberOfLastRows(){
        return ((Application.getNumberOfAds() % 9) / 3) + 1;
    }

    public static int getNumberOfRowsInLastPage(){
        if(Application.getNumberOfAds() % 9 == 0)
            return 3;
        else if(Application.getNumberOfAds() % 9 == 6)
            return 2;
        else if(Application.getNumberOfAds() % 9 == 3)
            return 1;
        else
            return ((Application.getNumberOfAds() % 9) / 3) + 1;
    }

    public static int getNumberOfAdsInLastRow(){
        return Application.getNumberOfAds() % 3;
    }
}

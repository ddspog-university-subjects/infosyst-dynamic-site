package controllers;

import play.GlobalSettings;
import play.Logger;

/**
 * Do the operations at start and at the end of the execution of server.
 */
public class Global extends GlobalSettings{

    @Override
    public void onStart(play.Application app){
        Logger.info("Aplicação inicializada...");
    }

    @Override
    public void onStop(play.Application app){
        Logger.info("Aplicação desligada...");
    }
}

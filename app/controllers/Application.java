package controllers;

import com.fasterxml.jackson.databind.node.ObjectNode;
import models.dao.AdDao;
import models.items.Ad;
import models.exceptions.InadequateParameterException;
import models.SearchResult;
import play.data.DynamicForm;
import play.data.Form;
import play.libs.Json;
import play.mvc.BodyParser;
import play.mvc.Controller;
import play.mvc.Result;

import java.util.List;

public class Application extends Controller {

    private static List<Ad> ads;
    private static SearchResult sr;
    private static AdDao dao;

    public static Result index(String any) {
        updateAds();
        return ok(views.html.index.render());
    }

    public static void updateAds() {
        dao = new AdDao();
        ads = dao.getList();
        try {
            sr = new SearchResult(ads);
        } catch (InadequateParameterException e) {
            throw new RuntimeException("The Search Result constructor receive a list od ads," +
                    " with problems when retrieved from database.");
        }
    }

    public static Result fromAllGetPage(Integer id){
        updateAds();
        return ok(views.html.pieces.ads.render(id, "all"));
    }

    public static Result fromSearchGetPage(){
        updateAds();

        DynamicForm request = Form.form().bindFromRequest();

        String t = request.get("tag");
        String o = request.get("option");
        Integer id = Integer.parseInt(request.get("id"));

        return getResultFromSearch(t, o, id);
    }

    public static Result getResultFromSearch(String t, String o, Integer id) {
        updateAds();
        switch (o){
            case "keyword":
                return searchUsingKeyword(t, id);
            case "instrument":
                return searchUsingInstrument(t, id);
            case "nice":
                return searchUsingNiceStyles(t, id);
            case "interest":
                return searchUsingInterest(t, id);
            default:
                return badRequest("Incorrect option given: " + o);
        }
    }

    private static Result searchUsingKeyword(String text, Integer id){
        try {
            sr.search(sr.viaKeyword(), text);
        } catch (Exception e) {
            return ok(views.html.pieces.unfound.render(id, "search/" + text + "/keyword",
                    "Desculpe, mas não conseguimos achar nenhum anúncio com a palavra-chave: " + text));
        }
        return ok(views.html.pieces.ads.render(id, "search/" + text + "/keyword"));
    }

    private static Result searchUsingInstrument(String text, Integer id){
        try {
            sr.search(sr.viaInstrument(), text);
        } catch (Exception e) {
            return ok(views.html.pieces.unfound.render(id, "search/" + text + "/instrument",
                    "Desculpe, mas não conseguimos achar nenhum anúncio com o instrumento: " + text));
        }
        return ok(views.html.pieces.ads.render(id, "search/" + text + "/instrument"));
    }

    private static Result searchUsingNiceStyles(String text, Integer id){
        try {
            sr.search(sr.viaNiceStyle(), text);
        } catch (Exception e) {
            return ok(views.html.pieces.unfound.render(id, "search/" + text + "/nice",
                    "Desculpe, mas não conseguimos achar nenhum anúncio com o estilo: " + text));
        }
        return ok(views.html.pieces.ads.render(id, "search/" + text + "/nice"));
    }

    private static Result searchUsingInterest(String text, Integer id){
        try {
            sr.search(sr.viaInterest(), text);
        } catch (Exception e) {
            return ok(views.html.pieces.unfound.render(id, "search/" + text + "/interest",
                    "Desculpe, mas não conseguimos achar nenhum anúncio com interesse em: " + text));
        }
        return ok(views.html.pieces.ads.render(id, "search/" + text + "/interest"));
    }

    public static Result createAd() throws InadequateParameterException {
        DynamicForm adRequest = Form.form().bindFromRequest();

        Ad ad = new Ad();
        ad.setTitle(adRequest.get("titleF"));
        ad.setDescription(adRequest.get("descriptionF"));
        ad.setCity(adRequest.get("cityF"));
        ad.setNeighborhood(adRequest.get("neighborhoodF"));
        ad.setInterestOnBand(Examples.getInterestRandom());
        ad.setContact(new String[]{adRequest.get("facebookF"), adRequest.get("emailF")});
        ad.setInstruments(Examples.getInstrumentsRandom());
        ad.setNiceStyles(Examples.getNiceStylesRandom());
        ad.setBadStyles(Examples.getBadStylesRandom());
        dao.add(ad);

        updateAds();
        return ok(views.html.pieces.ads.render(0, "all"));
    }

    @BodyParser.Of(BodyParser.Json.class)
    public static Result getTextJson(){
        ObjectNode result = Json.newObject();
        result.put("sidebarText", JSONPacks.getSidebar());
        result.put("searchBarText", JSONPacks.getSearchBar());
        result.put("headerText", JSONPacks.getHeader(15));
        result.put("hiddenText", JSONPacks.getHidden());
        result.put("btnDeleteText", JSONPacks.getBtnDelete());
        result.put("btnCreateAd", JSONPacks.getBtnCreateAd());
        result.put("modalCreateAd", JSONPacks.getModalCreateAd());
        result.put("adsSectionTitleText", JSONPacks.getSectionTitle());
        return ok(result);
    }

    public static int getNumberOfAds() {
        return sr.size();
    }

    public static Ad getAd(int i) throws InadequateParameterException {
        return sr.get(i);
    }
}

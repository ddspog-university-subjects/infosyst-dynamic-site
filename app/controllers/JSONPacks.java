package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import play.libs.Json;

/**
 * Packs the JSON request to put text on the website.
 */
public class JSONPacks {
    private static String title = "GIG CREATOR";
    private static String home = "Início";
    private static String seeAddsSection = "Ver Anúncios";
    private static String createAddsSection = "Criar Anúncios";
    private static String topMessage = "Anuncie-se, combine com mais músicos, e toque um grande show para todos nós.";
    private static String middleFirst = "Até o momento, ";
    private static String middleSecond = " de nossos anúncios já ajudaram a parceiros musicais se encontrarem.";
    private static String submit = "Submeter";
    private static String cancel = "Cancelar";
    private static String btnDeleteTitle = "Preencha o código para deletar o anúncio";
    private static String btnCreateAd = "Criar novo anúncio";
    private static String adsSectionTitle = "ANÚNCIOS";
    private static String searchBarLabel = "Buscar";
    private static String searchBarOption1Label = "Palavra-chave";
    private static String searchBarOption1Alias = "keyword";
    private static String searchBarOption2Label = "Instrumento";
    private static String searchBarOption2Alias = "instrument";
    private static String searchBarOption3Label = "Estilos legais";
    private static String searchBarOption3Alias = "nice";
    private static String searchBarOption4Label = "Interesse";
    private static String searchBarOption4Alias = "interest";
    private static String formAdCreationHeader = "Detalhe o seu anúncio";
    private static String formAdCreationTitleDescription = "Escreva aqui o título do anúncio:";
    private static String formAdCreationTitlePlaceholder = "Título";
    private static String formAdCreationDescriptionDescription = "Descreva seu perfil aqui:";
    private static String formAdCreationDescriptionPlaceholder = "Descrição";
    private static String formAdCreationCityDescription = "Digite o nome de sua cidade:";
    private static String formAdCreationCityPlaceholder = "Cidade";
    private static String formAdCreationNeighborhoodDescription = "Digite o nome de seu bairro:";
    private static String formAdCreationNeighborhoodPlaceholder = "Bairro";
    private static String formAdCreationFacebookDescription = "Preencha o link com o nome de seu usuário no Facebook:";
    private static String formAdCreationFacebookPlaceholder = "https://www.facebook.com/";
    private static String formAdCreationEmailDescription = "Digite o seu e-mail:";
    private static String formAdCreationEmailPlaceholder = "E-mail";

    public static JsonNode getSidebar(){
        ObjectNode result = Json.newObject();

        result.put("title", title);
        result.put("home", home);
        result.put("add", seeAddsSection);
        result.put("create", createAddsSection);

        return result;
    }

    public static JsonNode getHeader(int helped) {
        ObjectNode result = Json.newObject();

        result.put("title", title);
        result.put("topMessage", topMessage);
        result.put("middleFirst", middleFirst);
        result.put("middleSecond", middleSecond);
        result.put("helped", helped);

        return result;
    }

    public static JsonNode getHidden(){
        ObjectNode result = Json.newObject();

        result.put("submit", submit);

        return result;
    }

    public static JsonNode getBtnDelete() {
        ObjectNode result = Json.newObject();

        result.put("title", btnDeleteTitle);

        return result;
    }

    public static JsonNode getSectionTitle(){
        ObjectNode result = Json.newObject();

        result.put("title", adsSectionTitle);

        return result;
    }

    public static JsonNode getBtnCreateAd() {
        ObjectNode result = Json.newObject();

        result.put("text", btnCreateAd);

        return result;
    }

    public static JsonNode getSearchBar() {
        ObjectNode result = Json.newObject();

        result.put("label", searchBarLabel);
        result.put("options", getSearchBarOptions());
        result.put("submit", submit);

        return result;
    }

    private static JsonNode getSearchBarOptions() {
        ArrayNode result = new ArrayNode(JsonNodeFactory.instance);

        result.add(getSearchBarOption1());
        result.add(getSearchBarOption2());
        result.add(getSearchBarOption3());
        result.add(getSearchBarOption4());

        return result;
    }

    public static JsonNode getModalCreateAd(){
        ObjectNode result = Json.newObject();

        result.put("header", formAdCreationHeader);
        result.put("titleDescription", formAdCreationTitleDescription);
        result.put("titlePlaceholder", formAdCreationTitlePlaceholder);
        result.put("descriptionDescription", formAdCreationDescriptionDescription);
        result.put("descriptionPlaceholder", formAdCreationDescriptionPlaceholder);
        result.put("cityDescription", formAdCreationCityDescription);
        result.put("cityPlaceholder", formAdCreationCityPlaceholder);
        result.put("neighborhoodDescription", formAdCreationNeighborhoodDescription);
        result.put("neighborhoodPlaceholder", formAdCreationNeighborhoodPlaceholder);
        result.put("facebookDescription", formAdCreationFacebookDescription);
        result.put("facebookPlaceholder", formAdCreationFacebookPlaceholder);
        result.put("emailDescription", formAdCreationEmailDescription);
        result.put("emailPlaceholder", formAdCreationEmailPlaceholder);

        result.put("submit", submit);
        result.put("cancel", cancel);

        return result;
    }

    private static JsonNode getSearchBarOption1() {
        ObjectNode result = Json.newObject();

        result.put("label", searchBarOption1Label);
        result.put("alias", searchBarOption1Alias);

        return result;
    }

    private static JsonNode getSearchBarOption2() {
        ObjectNode result = Json.newObject();

        result.put("label", searchBarOption2Label);
        result.put("alias", searchBarOption2Alias);

        return result;
    }

    private static JsonNode getSearchBarOption3() {
        ObjectNode result = Json.newObject();

        result.put("label", searchBarOption3Label);
        result.put("alias", searchBarOption3Alias);

        return result;
    }

    private static JsonNode getSearchBarOption4() {
        ObjectNode result = Json.newObject();

        result.put("label", searchBarOption4Label);
        result.put("alias", searchBarOption4Alias);

        return result;
    }
}
